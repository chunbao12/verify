Verify 验证
===============

>引入verify.js文件
##使用方法
HTML代码中给需要验证的标签添加 lay-verify 属性；<br>

例：
~~~
<input type="text" lay-verify="required" >

~~~
js:
~~~
layui.use(['form', 'layer'], function () {
    var form = layui.form(), layer = layui.layer;

    //验证
    form.verify(verify);

    form.on('submit(submit)', function (data) {
        // 提交到方法
    })
});
~~~
多个验证：
~~~
lay-verify="required|ennum|range"
~~~
如需自定义验证：
~~~
layui.use(['form', 'layer'], function () {
    var form = layui.form(), layer = layui.layer;

    //自定义验证
    //verify.custom = [/正则规则/,'未通过时的描述信息'];
    //还可以这样写
    verify.custom = function(value,item){
        //value-验证表单的值
        //item-验证表单的dom对象
    }
    //验证
    form.verify(verify);

    form.on('submit(submit)', function (data) {
        // 提交到方法
    })
});
~~~
自定义验证用法：
~~~
lay-verify="custom"
~~~
##默认提供了以下验证规则

~~~
    required：'必填项'，
    phone：'手机号'
    ennum:'必须是字母或数字',
    en:'必须是英文字母',
    fname:'必须是中文',
    ftel:'必须是固定电话',
    carded | identity:'必须是18位身份证号码',
    ispostcode:'必须是中国邮政编码',
    isqq:'必须是腾讯QQ号码',
    isip:'必须是IPv4地址',
    isposfloat:'必须是正浮点数',
    isnegfloat:'必须是负浮点数',
    isfloat:'必须是浮点数',
    isemail | email:'必须是邮箱',
    isposint:'必须是正整数',
    isnegint:'必须是负整数',
    isint:'必须是整数',
    isdatetime:'必须是日期时间格式',//"YYYY-MM-DD HH:MM:SS" || "YYYY/MM/DD HH:MM:SS"
    isdate | date:'必须是日期格式', //YYYY-MM-DD || YYYY/MM/DD
    isdatestr : '必须是日期格式' //最强验证，31号，及 2月28日的验证.
                                                YYYY-MM-DD YYYY/MM/DD YYYY_MM_DD YYYY.MM.DD的形式
    istime:'必须是时间格式'   //HH:MM:SS
    url | isurl：'必须是URL地址'
    number | isnumber：'必须是数字'
    length ：'长度验证' //标签中需附加length属性，或 min-length:最小长度，max-length:最大程度
                        //length=? 指定长度
                        //min-length=? 最少长度
                        //max-length=? 最大长度
~~~

